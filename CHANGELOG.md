# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## 0.0.11 - 2018-11-08
### CHANGED:
- Add Changelog
- Fix typo in repo name
- Update package file
- Add ssh script
- Update readme
- Update version retrieval pattern
- Update build script output
- Remove login and push commands from build script
- Add build, push, versions scripts and add package.json for VC
- Update contrib docs
- Modify project structure and reorg
- Update node to 10.x branch
- Update CI image with gitflow-avh and 3 node tools

## 0.0.10 - 2018-03-25
### CHANGED:
- Update README
- Fix typo in gnupgs dep, add openssh-client dep

## 0.0.9 - 2018-03-25
### CHANGED:
- Add gnupgs and node to image

## 0.0.8 - 2018-03-18
### CHANGED:
- Add 'composer update' to run statement

## 0.0.7 - 2018-03-18
### CHANGED:
- Update dockerfile with git setup

## 0.0.6 - 2018-03-15
### CHANGED:
- Add README

## 0.0.5 - 2018-03-15
### CHANGED:
- Update CONTRIBUTING.md

## 0.0.4 - 2018-03-15
### CHANGED:
- Update readme for barrel ci

## 0.0.3 - 2018-03-15
### CHANGED:
- Update build commands

## 0.0.2 - 2018-03-15
### CHANGED:
- Add contribution guide

## 0.0.1 - 2018-03-15
### CHANGED:
- Initial commit
