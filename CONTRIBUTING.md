## Contributing Guide

This project uses a centralized git workflow. 
1. Simply fork the repo (or branch from master).
2. Open a merge request from your forked repo or branch to the master branch of this repo. 
3. Assign that merge request to @wturnerharris.
