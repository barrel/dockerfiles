# Barrel CI
#

FROM php:7.2-cli

COPY versions.sh /home/

ENV COMPOSER_ALLOW_SUPERUSER=1

# Install deps for all the things
RUN apt-get update -y && apt-get install -y jq git zip unzip gnupg openssh-client

# Install backstopJS (puppeteer) dependencies
RUN apt-get install -y libnss3 libcups2 libasound2 libx11-xcb1 libxcomposite1 \
  libxcursor1 libxdamage1 libxi6 libxtst6 libxss1 libxrandr2 \
  libatk1.0-0 libatk-bridge2.0-0 libpangocairo-1.0-0 libgtk-3-0

# Add nodejs
RUN curl -sL https://deb.nodesource.com/setup_10.x | bash - \
  && apt-get install -y nodejs

# Install Composer
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

# Install WP-CLI
RUN curl -O https://raw.githubusercontent.com/wp-cli/builds/gh-pages/phar/wp-cli.phar \
  && chmod +x wp-cli.phar && mv wp-cli.phar /usr/local/bin/wp \
  && alias wp='wp --allow-root'

# Install Terminus
RUN cd /home && curl -O https://raw.githubusercontent.com/pantheon-systems/terminus-installer/master/builds/installer.phar && php installer.phar install \
  && composer update

# Install Gitflow AVH
RUN curl --insecure --silent --location https://raw.githubusercontent.com/petervanderdoes/gitflow-avh/master/contrib/gitflow-installer.sh --output ./gitflow-installer.sh \
  && chmod u+x gitflow-installer.sh \
  && bash gitflow-installer.sh install stable \
  && rm gitflow-installer.sh

# Install Global Node Tools
RUN npm install -g eclint standard stylelint editorconfig-checker

# Install backstopjs separately
RUN npm install -g backstopjs --unsafe-perm=true --allow-root

# Cleanup
RUN mkdir -p /root/.terminus/cache \
  && apt-get -y clean \
  && apt-get -y autoclean \
  && apt-get -y autoremove \
  && rm -rf && rm -rf /var/lib/cache/* \
  && rm -rf /var/lib/log/* && rm -rf /tmp/*

# Setup SSH Config and Machine User
RUN mkdir -p ~/.ssh \
  && chmod 700 ~/.ssh \
  && printf "Host * \n    StrictHostKeyChecking no" > ~/.ssh/config  \
  && chmod 400 ~/.ssh/config  \
  && git config --global user.email "dev@barrelny.com"  \
  && git config --global user.name "Barrel Development" 

CMD ["/bin/bash"]
