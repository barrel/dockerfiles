## Barrel Multipurpose CI Docker Image

This repo hosts a multipurpose Docker build for Continuous Integration and Delivery for our agency. The applications installed on this docker image are used to facilitate code style compliance, testing, and deployment

See the [Contribution Guide](CONTRIBUTING.md) for more info on how to contribute.

### Image Details

This image is based on the official PHP 7.2 CLI image. It installs the following software:

- Latest OS Binaries for git (2.11.0), zip, unzip, gnupg, and openssh-client
- php (7.2.8)
- composer (1.7.3)
- wp (2.0.1)
- terminus (1.9.0)
- node (10.13.0)
- npm (6.4.1)
- standard (12.0.1)
- stylelint (9.7.1)

### Dockerhub and Variables

Below we use a few variables in case repos change ownership or names. We are hosted on [dockerhub](https://hub.docker.com/repository/docker/barrelny/multipurpose-ci).

Below you can see the values we assign to these variables in the build script.

```bash
GROUP="barrelny"
REPO="multipurpose-ci"
VERSION=$(sh version.sh)
```

The `$VERSION` is the current package.json version.

### Build Procedures

Run: `npm run build`

This happens in the script:

```bash
docker build -t $GROUP/$REPO .
docker tag $GROUP/$REPO $GROUP/$REPO:$VERSION
docker login
docker push $GROUP/$REPO:$VERSION
```

### Test Procedures

To see the versions of all target binaries installed to the image:

```bash
docker run -i -t $GROUP/$REPO:$VERSION /home/versions.sh
```

To ssh into the image and poke around with non-persistent data:

```bash
docker run -i -t $GROUP/$REPO:$VERSION /bin/bash
```

### Push to DockerHub

Execute in a terminal: `npm run push`

This happens in the script:

```bash
docker login
docker push $GROUP/$REPO:$VERSION
```
