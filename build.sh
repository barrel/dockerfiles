#!/bin/sh

GROUP="barrelny"
VERSION=$(sh version.sh)
REPO="multipurpose-ci"

docker build -t $GROUP/$REPO .
docker tag $GROUP/$REPO $GROUP/$REPO:$VERSION

echo "Outputting versions..."
docker run $GROUP/$REPO:$VERSION /home/versions.sh
