#!/bin/sh

GROUP="barrelny"
REPO="multipurpose-ci"
VERSION=$(sh version.sh)

echo "Outputting versions..."
docker run -i -t $GROUP/$REPO:$VERSION /home/versions.sh